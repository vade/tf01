# TensorFlow Image Classification Module

## Running the Trained Model
```
git clone https://bitbucket.org/vade/tf01
cd tf01/keras
jupyter notebook
```
Open in Jupyter: b3-predict.ipynb

## Resize Images before Training
Extract cars_train and cars_test datasets into keras folder
Open in Jupyter: b1-resize.ipynb

## Training the Model
Open in Jupyter: b2-model.ipynb

